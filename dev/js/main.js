/*! [PROJECT_NAME] | Suitmedia */

((window, document, undefined) => {

    const isActive = 'is-active'

    const path = {
        css: `${myPrefix}assets/css/`,
        js : `${myPrefix}assets/js/vendor/`
    }

    const assets = {
        _objectFit      : `${path.js}object-fit-images.min.js`
    }

    const Site = {
        enableActiveStateMobile() {
            if ( document.addEventListener ) {
                document.addEventListener('touchstart', () => {}, true)
            }
        },

        WPViewportFix() {
            if ( '-ms-user-select' in document.documentElement.style && navigator.userAgent.match(/IEMobile/) ) {
                let style = document.createElement('style')
                let fix = document.createTextNode('@-ms-viewport{width:auto!important}')

                style.appendChild(fix)
                document.head.appendChild(style)
            }
        },

        objectFitPolyfill() {
            load(assets._objectFit).then( () => {
                objectFitImages()
            })
        },

        globalToggler() {
            const $toggler = $('.global-content--toggle')
            const $target = $('.global-content')

            $toggler.on('click', () => {
                if ( !$target.hasClass(isActive) ){
                    $target.addClass(isActive)
                } else {
                    $target.removeClass(isActive)
                }
            })
        },

        hero() {
            const $hero = $('.hero-carousel')
            const $heroSearch = $('.hero-search')
            const $heroPrev = $('.hero-carousel-btn.--prev')
            const $heroNext = $('.hero-carousel-btn.--next')
            const $window = $(window)

            $hero.slick({
                dots: false,
                infinite: true,
                speed: 600,
                slidesToShow: 1,
                autoplay: true,
                arrows: false,
                fade: true,
                autoplaySpeed: 10000,
                draggable: false
            })

            $heroPrev.on('click', () => {
                $hero.slick('slickPrev')
            })
            
            $heroNext.on('click', () => {
                $hero.slick('slickNext')
            })

            $window.on('scroll', () => {
                let scrolled = $(window).scrollTop()

                $hero.css('transform', `translate3d( 0, ${scrolled*0.5}px, 0 )`)
                $heroSearch.css('transform', `translate3d( -50%, ${scrolled*0.5}px, 0)`)
            })
        },

        productCarousel() {
            const $productCarousel = $('.product-carousel')
            const $productNav = $('.product-nav-list')
            const $productNavItem = $('.product-nav-item')
            const $productPrev = $('.product-nav-button.--prev')
            const $productNext = $('.product-nav-button.--next')

            $productCarousel.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                speed: 500,
                draggable: false,
                asNavFor: $productNav
            })

            $productNav.slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                asNavFor: $productCarousel,
                dots: false,
                centerMode: false,
                focusOnSelect: true
            })

            $productCarousel.on('afterChange', function (){
                let slideNumber = $productCarousel.slick('slickCurrentSlide')
                let $currentSlide = $('.slick-current')

                $productNavItem.removeClass(isActive)
                $currentSlide.addClass(isActive)
            })

            $productPrev.on('click', () => {
                $productCarousel.slick('slickPrev')
                $productNav.slick('slickPrev')
            })

            $productNext.on('click', () => {
                $productCarousel.slick('slickNext')
                $productNav.slick('slickNext')
            })

        },

        search() {
            const $enter = $('.site-nav-search--enter')
            const $exit = $('.site-nav-search--exit')
            const $input = $('.site-nav-search--input')
            const $search = $('.site-nav-search')

            $enter.on('click', () => {
                $search.addClass(isActive)
                $input.focus()
            })

            $exit.on('click', () => {
                $search.removeClass(isActive)
                $input.val('')
            })
        },

        onePageNav() {
            const $onePageNavnav = $('#one-page-nav-list')

            $onePageNavnav.onePageNav({
                currentClass: isActive,
                changeHash: false,
                scrollSpeed: 750,
                scrollThreshold: 0.8,
                easing: 'swing'
            })
        },

        scrollReveal() {
            let slideUp = {
                duration: 850,
                distance: '0',
                scale: 0.8,
                delay: 150,
                reset: true,
                mobile:false
            };
            
            let slideLeft = {
                origin: 'right',
                duration: 600,
                distance: '200px',
                scale: 0.8,
                delay: 250,
                reset: true,
                mobile:false
            };
            
            let slideRight = {
                origin: 'left',
                duration: 600,
                distance: '200px',
                scale: 0.8,
                delay: 250,
                reset: true,
                mobile:false
            };
            
            window.sr = ScrollReveal();
            sr.reveal('.revealUp', slideUp);
            sr.reveal('.revealLeft', slideLeft);
            sr.reveal('.revealRight', slideRight);
        },

        globalMap() {
            let center = { lat: 27.112923, lng: 82.529297 }
            let pointers = [
                { lat: 27.503399, lng: 29.443359 },
                { lat: 29.969212, lng: 43.154297 },
                { lat: 24.342092, lng: 44.912109 },
                { lat: 27.112923, lng: 82.529297 },
                { lat: 27.191128, lng: 93.251953 },
                { lat: 24.021379, lng: 98.525391 },
                { lat: 39.210975, lng: 103.798828 },
                { lat: 23.860722, lng: 103.798828 },
                { lat: 17.617846, lng: 101.337891 },
                { lat: 15.088036, lng: 103.798828 },
                { lat: 13.726045, lng: 108.369141 },
                { lat: 6.287999, lng: 101.162109 },
                { lat: 2.959499, lng: 105.556641 }
            ]

            let map = new google.maps.Map(document.getElementById('globalMap'), {
                zoom: 3,
                center,
                scrollwheel: false
            })
            
            pointers.forEach( (current) => {
                let marker = new google.maps.Marker({
                    position: current,
                    icon: 'assets/img/logo-7_32.png',
                    map
                })
            })
        },

        footerMap() {
            let center = { lat: -6.126068, lng: 106.794721 }
            let map = new google.maps.Map(document.getElementById('footerMap'), {
                zoom: 18,
                center,
                scrollwheel: false
            })

            let marker = new google.maps.Marker({
                position: center,
                map
            })

        }
    }

    Promise.all([
        
    ]).then(() => {
        for (let fn in Site) {
            Site[fn]()
        }
        window.Site = Site
    })

    function exist(selector) {
        return new Promise((resolve, reject) => {
            let $elem = $(selector)

            if ( $elem.length ) {
                resolve($elem)
            } else {
                reject(`no element found for ${selector}`)
            }
        })
    }

    function load(url) {
        return new Promise((resolve, reject) => {
            Modernizr.load({
                load: url,
                complete: resolve
            })
        })
    }

    function loadJSON(url) {
        return new Promise((resolve, reject) => {
            fetch(url).then(res => {
                if ( res.ok ) {
                    resolve(res.json())
                } else {
                    reject('Network response not ok')
                }
            }).catch(e => {
                reject(e)
            })
        })
    }

})(window, document)
