<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta name="description" content="Baze is a front-end starter template">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">

    <meta property="og:url" content="">
    <meta property="og:title" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta property="og:type" content="website">
    <meta property="fb:app_id" content="">

    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:image" content="">

    <link rel="apple-touch-icon" href="assets/img/logo-7_32.png">
    <link rel="icon" type="image/png" href="assets/img/logo-7_32.png">

    <title>Sakura Filtration Product</title>

    <link rel="stylesheet" href="assets/css/main.css">
    <script src="assets/js/vendor/modernizr.min.js"></script>
</head>
<body>
    
    <header class="site-header">
        <a class="site-logo--container" href="#!">
            <img class="site-logo" src="assets/img/sakura_logo2.png" alt="">
        </a>
        <nav class="site-nav">
            <ul class="list-nostyle">
                <li class="site-nav-item">
                    <a href="#!">About Us</a>
                </li> 
                <li class="site-nav-item">
                    <a href="#!">New Product</a>
                </li>
                <li class="site-nav-item">
                    <a href="#!">Catalog</a>
                </li>
                <li class="site-nav-item">
                    <a href="#!">Events</a>
                </li>
                <li class="site-nav-item">
                    <a href="#!">Info</a>
                </li>
                <li class="site-nav-item">
                    <a href="#!">Distributor Networks</a>
                </li>
                <li class="site-nav-item site-nav-item--bordered">
                    <a href="#!"><small>EN</small> <i class="fa fa-angle-down"></i></a>
                </li>
                <li class="site-nav-item site-nav-item--bordered">
                    <a href="#!"><i class="fa fa-heart-o"></i></a>
                </li>
                <li class="site-nav-item site-nav-item--bordered">
                    <a href="#!"><i class="fa fa-user-o"></i></a>
                </li>
                <li class="site-nav-item site-nav-item--bordered">
                    <a class="site-nav-search--enter" href="#!"><i class="fa fa-search"></i></a>
                    <div class="site-nav-search">
                        <input class="site-nav-search--input" type="text" placeholder="Search">
                        <button class="site-nav-search--exit">&times;</button>
                    </div>
                </li>
            </ul>
        </nav>
        
        <nav class="one-page-nav">
            <ul class="list-nostyle" id="one-page-nav-list">
                <li class="one-page-nav-item is-active">
                    <a href="#hero"></a>
                </li>
                <li class="one-page-nav-item">
                    <a href="#global"></a>
                </li>
                <li class="one-page-nav-item">
                    <a href="#product"></a>
                </li>
                <li class="one-page-nav-item">
                    <a href="#news"></a>
                </li>
                <li class="one-page-nav-item">
                    <a href="#footer"></a>
                </li>
            </ul>
        </nav>

    </header>

    <main class="site-main">

        <section class="hero" id="hero">
            <div class="hero-carousel">
                <figure class="hero-carousel-item">
                    <div class="hero-carousel-img--container">
                        <img class="hero-carousel-img" src="assets/img/Home_Banner_1.jpg" alt="">
                    </div>
                    <figcaption class="hero-carousel-caption">
                        <h1>WHEN IT COMES TO FILTRATION FILTER OUT THE REST STAY WITH THE BEST</h1>
                        <p>Sakura Filters offer the widest range of quality products, available globally, and backed by excellent service</p>                        
                        <button class="hero-carousel-btn --prev"><i class="fa fa-angle-left fa-2x"></i></button>
                        <button class="hero-carousel-btn --next"><i class="fa fa-angle-right fa-2x"></i></button>
                    </figcaption>
                </figure>
                <figure class="hero-carousel-item">
                    <div class="hero-carousel-img--container">
                        <img class="hero-carousel-img" src="assets/img/Home_Banner_2.jpg" alt="">
                    </div>
                    <figcaption class="hero-carousel-caption">
                        <h1>LET NOTHING ELSE IN BUT CLEAN AIR</h1>
                        <p>Sakura Bio-Guard Cabin Air Filter with triple layer filtration provides cutting edge protection by removing viruses, bacteria, mold, allergens and other unwanted pollutants.</p>                        
                        <button class="hero-carousel-btn --prev"><i class="fa fa-angle-left fa-2x"></i></button>
                        <button class="hero-carousel-btn --next"><i class="fa fa-angle-right fa-2x"></i></button>
                    </figcaption>
                </figure>
                <figure class="hero-carousel-item">
                    <div class="hero-carousel-img--container">
                        <img class="hero-carousel-img" src="assets/img/Home_Banner_3.jpg" alt="">
                    </div>
                    <figcaption class="hero-carousel-caption">
                        <h1>OVER 40 YEARS OF EXPERIENCE IN FILTRATION INDUSTRY</h1>
                        <p>Our experiences and competencies as a global player will ensure that we continue to grow and serve our customers with the highest possible quality and valued products.</p>                        
                        <button class="hero-carousel-btn --prev"><i class="fa fa-angle-left fa-2x"></i></button>
                        <button class="hero-carousel-btn --next"><i class="fa fa-angle-right fa-2x"></i></button>
                    </figcaption>
                </figure>
            </div>
            <div class="hero-search">
                <i class="hero-search-icon fa fa-search fa-2x"></i>
                <input class="hero-search-input" type="text" placeholder="Search by keywords or a part number to find the filter you need">
                <button class="hero-search-submit btn">search</button>
            </div>
        </section>
        
        <section class="global" id="global">
            <div class="section-heading">
                <h2>SAKURA FILTER IS A BETTER CHOICE</h2>
                <p>For over 40 years, we have provided our customers with unbeatable products, in terms of value and quality, in well over 115 countries.</p>
                <p>We will meet the demands and needs of customers wherever they are in the world.</p>
            </div>
            <div class="global-map">
                <div class="global-mapobj" id="globalMap"></div>
                <div class="global-tag">
                    <h3>GLOBALLY AVAILABLE</h3>
                </div>
                <div class="global-content is-active">
                    <button class="global-content--toggle"></button>
                    <div class="bzg">
                        <div class="bzg_c" data-col="l6">
                            <h2 class="global-content--title">ONE STOP SHOP FOR QUALITY PRODUCT</h2>
                            <p>We cover the widest range of engine and non-engine applications on the market with more than 7.000 SKUs.</p>
                        </div>
                        <div class="bzg_c" data-col="l6">
                            <h2 class="global-content--title">QUALITY AND CONSISTENT PRODUCT</h2>
                            <p>Our Company, PT Selamat Sempurna Tbk, is the first Indonesian company to be awarded the ISO/TS 16949:2002. This certification allows the company to be a Tier 1 OEM Supplier</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="product" id="product">
            <div class="section-heading">
                <h2>SAKURA FILTERS ARE SIMPLY BUILT BETTER</h2>
                <p>All of our filter products are made by our companies that are certified to the ISO/IEC 17025:2005 to meet or exceed the quality, performance, and requirements of OEM/OES and aftermarket.</p>
            </div>
            <hr class="separator">
            <div class="product-carousel">
                <div class="media">
                    <figure class="media-figure">
                        <img src="assets/img/Sakura_Brochure_Side1-2.png" alt="">
                    </figure>
                    <article class="media-content">
                        <h1 class="product-carousel-title">SAKURA AIR FILTER</h1>
                        <p>It is designed and made to the exact standard to reduce frictional wear by eliminating the particulates release from frictional parts and combustion. This enhances efficiency and life expectancy of the engine.</p>
                        <button class="btn btn--arrow --red">SEARCH PRODUCT</button>
                    </article>
                </div>
                <div class="media">
                    <figure class="media-figure">
                        <img src="assets/img/Sakura_Brochure_Side1-2.png" alt="">
                    </figure>
                    <article class="media-content">
                        <h1 class="product-carousel-title">SAKURA OIL FILTER</h1>
                        <p>It is designed and made to the exact standard to reduce frictional wear by eliminating the particulates release from frictional parts and combustion. This enhances efficiency and life expectancy of the engine.</p>
                        <button class="btn btn--arrow --red">SEARCH PRODUCT</button>
                    </article>
                </div>
                <div class="media">
                    <figure class="media-figure">
                        <img src="assets/img/Sakura_Brochure_Side1-2.png" alt="">
                    </figure>
                    <article class="media-content">
                        <h1 class="product-carousel-title">SAKURA FUEL FILTER</h1>
                        <p>It is designed and made to the exact standard to reduce frictional wear by eliminating the particulates release from frictional parts and combustion. This enhances efficiency and life expectancy of the engine.</p>
                        <button class="btn btn--arrow --red">SEARCH PRODUCT</button>
                    </article>
                </div>
                <div class="media">
                    <figure class="media-figure">
                        <img src="assets/img/Sakura_Brochure_Side1-2.png" alt="">
                    </figure>
                    <article class="media-content">
                        <h1 class="product-carousel-title">SAKURA CABIN AIR FILTER</h1>
                        <p>It is designed and made to the exact standard to reduce frictional wear by eliminating the particulates release from frictional parts and combustion. This enhances efficiency and life expectancy of the engine.</p>
                        <button class="btn btn--arrow --red">SEARCH PRODUCT</button>
                    </article>
                </div>
            </div>
            <div class="product-nav">
                <button class="product-nav-button --prev"><i class="fa fa-chevron-left"></i></button>
                <button class="product-nav-button --next"><i class="fa fa-chevron-right"></i></button>
                <ul class="product-nav-list list-nostyle">
                    <li class="product-nav-item is-active">
                        <h3 class="product-nav-item-title">AIR FILTER</h3>
                        <div class="product-nav-item-image--container">
                            <img class="product-nav-item-image" src="assets/img/ege.jpg" alt="">
                        </div>
                    </li>
                    <li class="product-nav-item">
                        <h3 class="product-nav-item-title">OIL FILTER</h3>
                        <div class="product-nav-item-image--container">
                            <img class="product-nav-item-image" src="assets/img/Sakura_Brochure_Side1-2.png" alt="">
                        </div>
                    </li>
                    <li class="product-nav-item">
                        <h3 class="product-nav-item-title">FUEL FILTER</h3>
                        <div class="product-nav-item-image--container">
                            <img class="product-nav-item-image" src="assets/img/eaage.jpg" alt="">
                        </div>
                    </li>
                    <li class="product-nav-item">
                        <h3 class="product-nav-item-title">CABIN AIR FILTER</h3>
                        <div class="product-nav-item-image--container">
                            <img class="product-nav-item-image" src="assets/img/egae.jpg" alt="">
                        </div>
                    </li>
                </ul>
            </div>
        </section>

        <section class="news" id="news">
            <div class="section-heading">
                <h2>NEWS &amp; HIGHLIGHT</h2>
                <p>Welcome to the world of Sakura and our latest updates</p>
            </div>
            <hr class="separator">
            <ul class="news-cards list-nostyle">
                <li class="news-card revealLeft">
                    <a href="#!">
                        <figure class="news-card-img--container">
                            <img class="news-card-img" src="assets/img/InvestorAwards2016.jpg" alt="">
                        </figure>
                        <article class="news-card-content">
                            <h3>PT Selamat Sempurna Tbk (SMSM) records the achievement of receiving the Investor Awards 2016</h3>
                            <time class="news-time">10 June 2016</time>
                            <p>This award is a proof and an appreciation for the company's commitment and this will motivate us to grow continually and bring positive contributions to all stakeholders.</p>
                        </article>
                    </a>
                </li>
                <li class="news-card revealUp">
                    <a href="#!">
                        <figure class="news-card-img--container">
                            <img class="news-card-img" src="assets/img/PrimaniyartaAward2016.jpg" alt="">
                        </figure>
                        <article class="news-card-content">
                            <h3>Primaniyarta Award 2016</h3>
                            <time class="news-time">12 June 2016</time>
                            <p>Achievement in 2016 has completed the award collection of PT SMSM. We have obtained Primaniyarta Award for seven times since 2009 and also: for the 6th year in a row.</p>
                        </article>
                    </a>
                </li>
                <li class="news-card revealRight">
                    <a href="#!">
                        <figure class="news-card-img--container">
                            <img class="news-card-img" src="assets/img/EcoDriving2017.jpg" alt="">
                        </figure>
                        <article class="news-card-content">
                            <h3>Eco Driving 2017</h3>
                            <time class="news-time">12 June 2016</time>
                            <p>Congratulation and big thanks to Mark Darwin, Fitra Eri Rueben Wong and ALL SAKURA-TEDCO RACING TEAM. Due to our hard work, we won the race!</p>
                        </article>
                    </a>
                </li>
            </div>
        </section>
    
    </main>

    <footer id="footer">
        <div class="site-footer-map" id="footerMap"></div>
        <div class="site-footer-container">
            <p class="site-footer-quote">Whether you have a need for a quality Filter or are looking for Distributorship and Private Branding, we are here to help. Feel<br>free to contact us for All Automotive &amp; Heavy Duty Product, including OEM or Specially Designed Products.</p>
            <div class="bzg">
                <div class="bzg_c" data-col="l8">
                    <form class="bzg">
                        <div class="bzg_c" data-col="l6">
                            <div class="site-footer-form --name form__row">
                                <input class="form-input" type="text" placeholder="Name">
                            </div>
                            <div class="site-footer-form --mail form__row">
                                <input class="form-input" type="email" placeholder="Email">
                            </div>
                            <div class="site-footer-form --phone form__row">
                                <input class="form-input" type="number" placeholder="Phone">
                            </div>
                        </div>
                        <div class="bzg_c" data-col="l6">
                            <div class="site-footer-form form__row">
                                <textarea class="form-input" rows="6" placeholder="Type Your Message or Question Here"></textarea>
                            </div>
                        </div>
                        <div class="bzg_c" data-col="l12">
                            <button class="btn btn--arrow --white">SUBMIT</button>
                        </div>
                    </form>
                </div>
                <div class="bzg_c" data-col="l4">
                    <span class="site-footer-dropdown">Sales &amp; Marketing Office <i class="fa fa-angle-down"></i></span>
                    <div class="site-footer-address">
                        <address>Komplek Industri ADR Desa Kadujaya, Curug Tangerang, Banten INDONESIA</address>
                        <p>Phone : (62-21) 5949 2169<br>Fax: (62-21) 5949 2168<br>Email:  sales.marketing@adrgroup.com</p>
                    </div>
                </div>
            </div>
            <div class="site-footer-copyright">
                <ul class="list-nostyle list-inline">
                    <li><a class="site-footer-media" href="#!"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="site-footer-media" href="#!"><i class="fa fa-twitter"></i></a></li>
                    <li><a class="site-footer-media" href="#!"><i class="fa fa-instagram"></i></a></li>
                    <li><a class="site-footer-media" href="#!"><i class="fa fa-youtube-play"></i></a></li>
                </ul>
                <p>Related Link | &copy;2016 ADR Group of Companies.</p>
            </div>
        </div>
    </footer>

    <script>window.myPrefix = '';</script>
    <script src="assets/js/vendor/jquery.min.js"></script>
    <script src="assets/js/vendor/slick.min.js"></script>
    <script src="assets/js/vendor/scrollreveal.min.js"></script>
    <script src="assets/js/vendor/jquery.nav.min.js"></script>
    <script src="https://cdn.polyfill.io/v2/polyfill.js?features=default,promise,fetch"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9YavfvQiEuyekTewaT8X3IxFEJ6I8kLI"></script>
    <script src="assets/js/main.min.js"></script>
</body>
</html>
